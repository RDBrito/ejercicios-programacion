#include <stdio.h>
#include <stdlib.h>


float pide_numero_real();
int validar_numero_real(char numero[]);

int main(int argc, char* argv[])
{
    float lado = 0;
    float perimetro = 0;

    printf("\n\n\t%c====================================================================%c",201,187);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c    Este programa calcula el per%cmetro de un tri%cngulo equil%ctero   %c",186,161,160,160,186);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c%c==================================================================%c%c\n\n\n",200,205,205,188);
    do{
        printf("\n  >> Ingresa la longitud de un lado: ");
        lado = pide_numero_real();
        if(lado == 0){
            printf("\n\n  La longitud del lado debe ser mayor a 0.\n");
        }    	
    }while(lado == 0);
    perimetro = lado*3;
    printf("\n\n\n  El per%cmetro del tri%cngulo equil%ctero es: \n\n\t%.3f [unidades]   \n\n\n",161,160,160,perimetro);
    return 0;
}




//-----Funci�n que pide un n�mero real-------//
float pide_numero_real()
{
    char numero[5];
    int N;
    float numerovalido;
    do{
        scanf("%s",numero);
        N=validar_numero_real(numero);
    }while(N==0);

    numerovalido=atof(numero);
    return numerovalido;
}

//------Funci�n que valida que el n�mero sea real positivo-------// 
int validar_numero_real(char numero[]){
    int i;
    int punto = 0;
    
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i]))){
            if(numero[i] == '.'){
                punto++;
            }
            else{
        	 printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        	 return 0;
            }
        }
    }
    if(punto >=2){
        printf("\n  %s no es un n%cmero valido",numero,163);
        printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        return 0;
    }
    return 1;
}