#include <stdio.h>
#include <stdlib.h>


float pide_numero_real();
int validar_numero_real(char numero[]);
int valida_triangulo(float ladoA, float ladoB, float ladoC);

int main(int argc, char* argv[])
{
    float ladoA = 0;
    float ladoB = 0;
    float ladoC = 0;
    float perimetro = 0;

    printf("\n\n\t%c====================================================================%c",201,187);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c    Este programa calcula el per%cmetro de un tri%cngulo escaleno     %c",186,161,160,186);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c%c==================================================================%c%c\n\n\n",200,205,205,188);
    do{
        printf("\n  >> Ingresa la longitud del primer lado (a): ");
        ladoA = pide_numero_real();
        printf("\n  >> Ingresa la longitud del segundo lado (b): ");
        ladoB = pide_numero_real();
        printf("\n  >> Ingresa la longitud del tercer lado (c): ");
        ladoC = pide_numero_real();
        if(ladoA == 0 || ladoB == 0 || ladoC == 0){
            printf("\n\n  Los tres lados deben tener longitudes mayores a 0.\n");
        }
        if(valida_triangulo(ladoA, ladoB, ladoC) != 2){
            printf("\n\n\n  No es posible formar un tri%cngulo escaleno con las longitudes ingresadas.\n",161,162);
            if(valida_triangulo(ladoA, ladoB, ladoC) == 0){
                printf("  Las longitudes corresponden a un tri%cngulo equil%ctero.\n",160,160);
            }else if(valida_triangulo(ladoA, ladoB, ladoC) == 1){
                printf("  Las longitudes corresponden a un tri%cngulo is%csceles.\n",160,162);
            }
        }
    }while(  (ladoA == 0 || ladoB == 0 || ladoC == 0)      ||     (valida_triangulo(ladoA, ladoB, ladoC) != 2) );
    perimetro = ladoA + ladoB + ladoC;
    printf("\n\n\n  El per%cmetro del tri%cngulo escaleno es: \n\n\t%.3f [unidades]   \n\n\n",161,160,perimetro);
    return 0;
}



//----------Función que recibe tres segmentos y determina si se puede construir un triángulo equilátero, isósceles o escaleno--------//
int valida_triangulo(float ladoA, float ladoB, float ladoC)
{

    if( ladoA < (ladoB+ladoC) ){
        if( ladoB < (ladoA+ladoC) ){
            if( ladoC < (ladoA+ladoB) ){
                if( (ladoA == ladoB) &&  (ladoA == ladoC) && (ladoB == ladoC) ){
                    return 0;
                }else if( (ladoA == ladoB) ||  (ladoA == ladoC) || (ladoB == ladoC) ){
                    return 1;
                }else{
                    return 2;
                }    			
            }else{
                return -1;
            }
        }else{
            return -1;
        }
    }else{
        return -1;
    }
    
}



//-----Función que pide un número real-------//
float pide_numero_real()
{
    char numero[5];
    int N;
    float numerovalido;
    do{
        scanf("%s",numero);
        N=validar_numero_real(numero);
    }while(N==0);

    numerovalido=atof(numero);
    return numerovalido;
}



//------Función que valida que el número sea real positivo-------//
int validar_numero_real(char numero[]){
    int i;
    int punto = 0;
    
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i]))){
            if(numero[i] == '.'){
                punto++;
            }
            else{
        	 printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        	 return 0;
            }
        }
    }
    if(punto >=2){
        printf("\n  %s no es un n%cmero valido",numero,163);
        printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        return 0;
    }
    return 1;
}