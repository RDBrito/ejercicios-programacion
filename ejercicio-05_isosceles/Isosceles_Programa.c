#include <stdio.h>
#include <stdlib.h>


float pide_numero_real();
int validar_numero_real(char numero[]);
int valida_triangulo(float ladoA, float ladoB, float ladoC);

int main(int argc, char* argv[])
{
    float base = 0;
    float lado = 0;
    float perimetro = 0;

    printf("\n\n\t%c====================================================================%c",201,187);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c    Este programa calcula el per%cmetro de un tri%cngulo is%csceles    %c",186,161,160,162,186);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c%c==================================================================%c%c\n\n\n",200,205,205,188);
    do{
        printf("\n  >> Ingresa la longitud de la base: ");
        base = pide_numero_real();
        printf("\n  >> Ingresa la longitud de un lado: ");
        lado = pide_numero_real();
        if(base == 0 || lado == 0){
            printf("\n\n  El lado y la base deben ser longitudes mayores a 0.\n");
        }
        if(valida_triangulo(lado, lado, base) != 1){
            printf("\n\n  No es posible formar un tri%cngulo is%csceles con las longitudes ingresadas.\n",161,162);
        }
    }while( (base == 0 || lado == 0)  ||  (valida_triangulo(lado, lado, base) != 1) );
    perimetro = base + lado + lado;
    printf("\n\n\n  El per%cmetro del tri%cngulo is%csceles es: \n\n\t%.3f [unidades]   \n\n\n",161,160,162,perimetro);
    return 0;
}


//----------Función que recibe tres segmentos y determina si se puede construir un triángulo equilátero, isósceles o escaleno--------//
int valida_triangulo(float ladoA, float ladoB, float ladoC)
{

    if( ladoA < (ladoB+ladoC) ){
        if( ladoB < (ladoA+ladoC) ){
            if( ladoC < (ladoA+ladoB) ){
                if( (ladoA == ladoB) &&  (ladoA == ladoC) && (ladoB == ladoC) ){
                    return 0;
                }else if( (ladoA == ladoB) ||  (ladoA == ladoC) || (ladoB == ladoC) ){
                    return 1;
                }else{
                    return 2;
                }    			
            }else{
                return -1;
            }
        }else{
            return -1;
        }
    }else{
        return -1;
    }
    
}



//-----Función que pide un número real-------//
float pide_numero_real()
{
    char numero[5];
    int N;
    float numerovalido;
    do{
        scanf("%s",numero);
        N=validar_numero_real(numero);
    }while(N==0);

    numerovalido=atof(numero);
    return numerovalido;
}



//------Función que valida que el número sea real positivo-------//
int validar_numero_real(char numero[]){
    int i;
    int punto = 0;
    
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i]))){
            if(numero[i] == '.'){
                punto++;
            }
            else{
        	 printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        	 return 0;
            }
        }
    }
    if(punto >=2){
        printf("\n  %s no es un n%cmero valido",numero,163);
        printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        return 0;
    }
    return 1;
}