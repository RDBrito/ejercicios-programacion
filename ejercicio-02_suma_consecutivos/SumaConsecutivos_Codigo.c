#include <stdio.h>
#include <stdlib.h>

int pide_numero_entero();
int validar_numero_entero(char numero[]);

int main(int argc, char* argv[])
{
    int numero = 0;
    int sumaConsecutivos = 0;

    printf("\n\n\t%c================================================================%c",201,187);
    printf("\n\t%c                                                                %c",186,186);
    printf("\n\t%c    Este programa muestra la suma de los n%cmeros consecutivos   %c",186,163,186);
    printf("\n\t%c             desde el 1 hasta el n%cmero ingresado.              %c",186,163,186);
    printf("\n\t%c                                                                %c",186,186);
    printf("\n\t%c%c==============================================================%c%c\n\n\n",200,205,205,188);
    do{
        printf("\n  >> Ingrese un n%cmero entero entre 1 y 50: ",163);
        numero = pide_numero_entero();
    }while((numero <1) || (numero >50));
    sumaConsecutivos = (numero*(numero+1))/2;
    printf("\n\n\n  %c La suma de los n%cmeros consecutivos desde el 1 hasta %d es: %d\n\n\n",219,163, numero, sumaConsecutivos);
    return 0;
}




//-------Solicita N�mero Entero al usuario------//
int pide_numero_entero() 
{
   char numero[5];
   int N = 0;
   int numero_valido = 0;
   do
   {
       scanf("%s",numero);
       N=validar_numero_entero(numero);

   }while(N==0);

   numero_valido=atoi(numero);
   return numero_valido;
}

//-------Valida que el numero solo se componga de enteros------//
int validar_numero_entero(char numero[])  
{
    int i = 0;
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i])))
        {
            printf("\n  %c Ingrese %cnicamente un n%cmero entero positivo: ",207,163,163);
            return 0;
        }
    }
    return 1;
}
