#include <stdio.h>
#include <stdlib.h>


float pide_numero_real();
int validar_numero_real(char numero[]);
int valida_triangulo(float ladoA, float ladoB, float ladoC);
void perimetro_escaleno();
void perimetro_isosceles();
void perimetro_equilatero();

int main(int argc, char* argv[])
{
    int opcion = 0;

    do{
        printf("\n\n\t%c======================================================================%c",201,187);
        printf("\n\t%c                                                                      %c",186,186);
        printf("\n\t%c    Este programa calcula el per%cmetro de un tri%cngulo elegido por    %c",186,161,160,186);
        printf("\n\t%c          el usuario (escaleno, is%csceles o equil%ctero)               %c",186,162,160,186);
        printf("\n\t%c                                                                      %c",186,186);
        printf("\n\t%c%c====================================================================%c%c\n\n\n",200,205,205,188);
	    
        printf("\n   Men%c\n",163);
        printf("   -----------\n\n");
        printf("   1. per%cmetro de un tri%cngulo equil%ctero\n",161,160,160);
        printf("   2. per%cmetro de un tri%cngulo is%csceles\n",161,160,162);
        printf("   3. per%cmetro de un tri%cngulo escaleno\n\n",161,160);
        printf("   4. salir\n\n\n");
	
        do{
            printf("\n  >> Ingrese una opci%cn: ",162);
            opcion = (int)pide_numero_real();
        }while( opcion<1 || opcion>4 );
	    
        switch (opcion){
            case 1:
                system("cls");
                perimetro_equilatero();
                break;
            case 2:
                system("cls");
                perimetro_isosceles();
                break;
            case 3:
                system("cls");
                perimetro_escaleno();
                break;
            case 4:
                break;
        }
    }while(opcion != 4);

    return 0;
}



//----------Función que recibe tres segmentos y determina si se puede construir un triángulo equilátero, isósceles o escaleno--------//
int valida_triangulo(float ladoA, float ladoB, float ladoC)
{

    if( ladoA < (ladoB+ladoC) ){
        if( ladoB < (ladoA+ladoC) ){
            if( ladoC < (ladoA+ladoB) ){
                if( (ladoA == ladoB) &&  (ladoA == ladoC) && (ladoB == ladoC) ){
                    return 0;
                }else if( (ladoA == ladoB) ||  (ladoA == ladoC) || (ladoB == ladoC) ){
                    return 1;
                }else{
                    return 2;
                }    			
            }else{
                return -1;
            }
        }else{
            return -1;
        }
    }else{
        return -1;
    }
    
}



//-----Función que pide un número real-------//
float pide_numero_real()
{
    char numero[5];
    int N;
    float numerovalido;
    do{
        scanf("%s",numero);
        N=validar_numero_real(numero);
    }while(N==0);

    numerovalido=atof(numero);
    return numerovalido;
}



//------Función que valida que el número sea real positivo-------//
int validar_numero_real(char numero[]){
    int i;
    int punto = 0;
    
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i]))){
            if(numero[i] == '.'){
                punto++;
            }
            else{
        	    printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        	    return 0;
            }
        }
    }
    if(punto >=2){
        printf("\n  %s no es un n%cmero valido",numero,163);
        printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        return 0;
    }
    return 1;
}



//--------Función que se encarga de calcular el perímetro de un triángulo escaleno-----//
void perimetro_escaleno()
{
    float ladoA = 0;
    float ladoB = 0;
    float ladoC = 0;
    float perimetro = 0;
    
    printf("\n\n\t%c====================================================================%c",201,187);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c                          opci%cn elegida: 3                         %c",186,162,186);
    printf("\n\t%c                 per%cmetro de un tri%cngulo escaleno                 %c",186,161,160,186);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c%c==================================================================%c%c\n\n\n",200,205,205,188);
    do{
        printf("\n  >> Ingresa la longitud del primer lado (a): ");
        ladoA = pide_numero_real();
        printf("\n  >> Ingresa la longitud del segundo lado (b): ");
        ladoB = pide_numero_real();
        printf("\n  >> Ingresa la longitud del tercer lado (c): ");
        ladoC = pide_numero_real();
        if(ladoA == 0 || ladoB == 0 || ladoC == 0){
            printf("\n\n  Los tres lados deben tener longitudes mayores a 0.\n");
        }
        if(valida_triangulo(ladoA, ladoB, ladoC) != 2){
            printf("\n\n\n  No es posible formar un tri%cngulo escaleno con las longitudes ingresadas.\n",161,162);
            if(valida_triangulo(ladoA, ladoB, ladoC) == 0){
                printf("  Las longitudes corresponden a un tri%cngulo equil%ctero.\n",160,160);
            }else if(valida_triangulo(ladoA, ladoB, ladoC) == 1){
                printf("  Las longitudes corresponden a un tri%cngulo is%csceles.\n",160,162);
            }
        }
    }while(  (ladoA == 0 || ladoB == 0 || ladoC == 0)      ||     (valida_triangulo(ladoA, ladoB, ladoC) != 2) );
    perimetro = ladoA + ladoB + ladoC;
    printf("\n\n\n  El per%cmetro del tri%cngulo escaleno es: \n\n\t%.3f [unidades]   \n\n\n",161,160,perimetro);
    printf("\n\n\t\t          presione ENTER para continuar");
    getchar();
    getchar();
    system("cls");
}




//--------Función que se encarga de calcular el perímetro de un triángulo isósceles----//
void perimetro_isosceles()
{
    float base = 0;
    float lado = 0;
    float perimetro = 0;

    printf("\n\n\t%c====================================================================%c",201,187);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c                          opci%cn elegida:  2                        %c",186,162,186);
    printf("\n\t%c                 per%cmetro de un tri%cngulo is%csceles                %c",186,161,160,162,186);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c%c==================================================================%c%c\n\n\n",200,205,205,188);
    do{
        printf("\n  >> Ingresa la longitud de la base: ");
        base = pide_numero_real();
        printf("\n  >> Ingresa la longitud de un lado: ");
        lado = pide_numero_real();
        if(base == 0 || lado == 0){
            printf("\n\n  El lado y la base deben ser longitudes mayores a 0.\n");
        }
        if(valida_triangulo(lado, lado, base) != 1){
            printf("\n\n  No es posible formar un tri%cngulo is%csceles con las longitudes ingresadas.\n",161,162);
        }
    }while( (base == 0 || lado == 0)  ||  (valida_triangulo(lado, lado, base) != 1) );
    perimetro = base + lado + lado;
    printf("\n\n\n  El per%cmetro del tri%cngulo is%csceles es: \n\n\t%.3f [unidades]   \n\n\n",161,160,162,perimetro);
    printf("\n\n\t\t          presione ENTER para continuar");
    getchar();
    getchar();
    system("cls");
}



//--------Función que se encarga de calcular el perímetro de un triángulo equilátero----//
void perimetro_equilatero()
{
    float lado = 0;
    float perimetro = 0;

    printf("\n\n\t%c====================================================================%c",201,187);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c                          opci%cn elegida:  1                        %c",186,162,186);
    printf("\n\t%c                 per%cmetro de un tri%cngulo equil%ctero               %c",186,161,160,160,186);
    printf("\n\t%c                                                                    %c",186,186);
    printf("\n\t%c%c==================================================================%c%c\n\n\n",200,205,205,188);
    do{
        printf("\n  >> Ingresa la longitud de un lado: ");
        lado = pide_numero_real();
        if(lado == 0){
            printf("\n\n  La longitud del lado debe ser mayor a 0.\n");
        }    	
    }while(lado == 0);
    perimetro = lado*3;
    printf("\n\n\n  El per%cmetro del tri%cngulo equil%ctero es: \n\n\t%.3f [unidades]   \n\n\n",161,160,160,perimetro);
    printf("\n\n\t\t          presione ENTER para continuar");
    getchar();
    getchar();
    system("cls");
}