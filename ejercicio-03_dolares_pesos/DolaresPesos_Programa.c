#include <stdio.h>
#include <stdlib.h>


float pide_numero_real();
int validar_numero_real(char numero[]);

int main(int argc, char* argv[])
{
    float DOLAR_A_PESO = 21.97;
    float montoDolares = 0;
    float montoPesos = 0;
    char *fecha = "16 de agosto 2020.";
	
    printf("\n\n\t%c===================================================================%c",201,187);
    printf("\n\t%c                                                                   %c",186,186);
    printf("\n\t%c    Este programa recibe un monto en d%clares (USD) y devuelve la   %c",186,162,186);
    printf("\n\t%c            cantidad equivalente en pesos mexicanos (MXN)          %c",186,186);		
    printf("\n\t%c                                                                   %c",186,186);
    printf("\n\t%c%c=================================================================%c%c\n\n\n",200,205,205,188);
    printf("\n\n  %s",fecha);
    printf("\n  1 d%clar estadounidense es igual a %.2f peso mexicano",162,DOLAR_A_PESO);
    printf("\n\n  >> Ingrese su monto en d%clares (USD): ",162);    

    montoDolares = pide_numero_real();
    montoPesos = montoDolares*DOLAR_A_PESO;
    
    printf("\n\n\n  d%clar estadounidense:  %.2f  (USD)",162, montoDolares);
    printf("\n  peso mexicano:         %.2f  (MXN)\n\n\n", montoPesos);
    
    
    return 0;
}






//Funciones que se encargan de pedir y verificar un n�mero al usuario
float pide_numero_real()
{
    char numero[5];
    int N;
    float numerovalido;
    do{
        scanf("%s",numero);
        N=validar_numero_real(numero);
    }while(N==0);

    numerovalido=atof(numero);
    return numerovalido;
}

int validar_numero_real(char numero[]){
    int i;
    int punto = 0;
    
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i]))){
            if(numero[i] == '.'){
                punto++;
            }
            else{
        	 printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,164);
        	 return 0;
            }
        }
    }
    if(punto >=2){
        printf("\n  %s no es un n%cmero valido",numero,163);
        printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        return 0;
    }
    return 1;
}
