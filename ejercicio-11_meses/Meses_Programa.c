#include <stdio.h>
#include <stdlib.h>

int pide_numero_entero();
int validar_numero_entero(char numero[]);

int main(int argc, char* argv[])
{
    int numero_mes = 0;
    char *meses[] = { "Enero", "Febrero", "Mazro", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };

    printf("\n\n\t%c==================================================================%c",201,187);
    printf("\n\t%c                                                                  %c",186,186);
    printf("\n\t%c    Este programa recibe un n%cmero entero entre 0 y 11 y debe     %c",186,163,186);
    printf("\n\t%c           devolver el nombre del mes correspondiente.            %c",186,186);
    printf("\n\t%c        (Toma en cuenta que 0 = Enero y 11 = Diciembre.)          %c",186,186);
    printf("\n\t%c                                                                  %c",186,186);
    printf("\n\t%c%c================================================================%c%c\n\n\n",200,205,205,188);
    
    do{
        printf("\n  >> Ingrese un n%cmero: ",163);
        numero_mes = pide_numero_entero();
        if( numero_mes > 11 ){
            printf("\n\n  Solo se pueden ingresar n%cmeros del 0 al 11.",163);
        }    	
    }while(  numero_mes > 11 );
    printf("\n\n\t%c=========================%c",201,187);
    printf("\n\t          %s  ",meses[numero_mes]); 
    printf("\n\t%c%c=======================%c%c\n\n",200,205,205,188); 
    printf("\n\n"); 

    return 0;    
}



//-------Solicita Número Entero al usuario------//
int pide_numero_entero() 
{
   char numero[5];
   int N = 0;
   int numero_valido = 0;
   do
   {
       scanf("%s",numero);
       N=validar_numero_entero(numero);

   }while(N==0);

   numero_valido=atoi(numero);
   return numero_valido;
}

//-------Valida que el numero solo se componga de enteros------//
int validar_numero_entero(char numero[])  
{
    int i = 0;
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i])))
        {
            printf("\n  %c Ingrese %cnicamente un n%cmero entero positivo: ",207,163,163);
            return 0;
        }
    }
    return 1;
}