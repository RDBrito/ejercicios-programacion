#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

int pide_numero_entero();
int validar_numero_entero(char numero[]);
void reglas();
void aguila();
void sol();
int lanzamiento(int eleccion_player,int apuesta_cpu,int apuesta_player);
void perdiste();
void ganaste();

int main(int argc, char* argv[])
{
	int volado = 0;
	int bolsa_cpu = 500;
	int bolsa_player = 500;
	int eleccion_player = 0;
	int apuesta_cpu = 0;
	int apuesta_player = 0;
	int ronda = 1;
	int victorias_player = 0;
	int victorias_cpu = 0;
	reglas();
	
	
	srand(time(NULL));
	do{
		printf("\n\n  | Ronda %d |",ronda);
		printf("\n\n  Bolsa CPU: $%d                T%c Bolsa: $%d",bolsa_cpu,163,bolsa_player );
        apuesta_cpu = 20 + rand() % 81;
        printf("\n\n\n  >> CPU apuesta $%d",apuesta_cpu);
	    do{
	        printf("\n\n  >> Ingrese su apuesta: ");
	        apuesta_player = pide_numero_entero();
	        if(apuesta_player < 20 || apuesta_player >100){
	            printf("\n\n  Las apuestas solo pueden ir de $20 a $100.",161,164);
	        }    	
	    }while(apuesta_player < 20 || apuesta_player >100);   
        printf("\n\n\n  %c El volado ser%c lanzado, elija.",219,160);
		do{
			printf("\n  %c [1.%cguila / 2.Sol]: ",219,181);
			eleccion_player = pide_numero_entero();
		}while( eleccion_player < 1  || eleccion_player > 2);
		volado = lanzamiento(eleccion_player, apuesta_cpu, apuesta_player);
        if(volado == eleccion_player){
        	victorias_player++;
        	bolsa_player = bolsa_player + apuesta_cpu;
        	bolsa_cpu = bolsa_cpu - apuesta_cpu;
        }else{
        	victorias_cpu++;
        	bolsa_cpu = bolsa_cpu + apuesta_player;
        	bolsa_player = bolsa_player - apuesta_player;
        }
        ronda++;
	}while(ronda < 4);
	if(victorias_player >= 2){
	    printf("\n\n\n  __________________________________________________________________________\n");
	    ganaste();
	    printf("\n\n\n\n    %c===================================================================%c",201,187);
	    printf("\n       Bolsa CPU: $%d     Bolsa del jugador: $%d    Rondas ganadas: %d",bolsa_cpu,bolsa_player,victorias_player);
	    printf("\n    %c%c=================================================================%c%c\n",200,205,205,188);
	    printf("\n  __________________________________________________________________________\n\n\n");
	}else{
	    printf("\n\n\n  __________________________________________________________________________\n");
	    perdiste();
	    printf("\n\n\n\n    %c===================================================================%c",201,187);
	    printf("\n       Bolsa CPU: $%d     Bolsa del jugador: $%d    Rondas ganadas: %d",bolsa_cpu,bolsa_player,victorias_cpu);
	    printf("\n    %c%c=================================================================%c%c\n",200,205,205,188);
	    printf("\n  __________________________________________________________________________\n\n\n");		
	}
	

    return 0;
}



//-------Solicita Número Entero al usuario------//
int pide_numero_entero() 
{
   char numero[5];
   int N = 0;
   int numero_valido = 0;
   do
   {
       scanf("%s",numero);
       N=validar_numero_entero(numero);

   }while(N==0);

   numero_valido=atoi(numero);
   return numero_valido;
}

//-------Valida que el numero solo se componga de enteros------//
int validar_numero_entero(char numero[])  
{
    int i = 0;
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i])))
        {
            printf("\n  %c Ingrese %cnicamente un n%cmero entero positivo: ",207,163,163);
            return 0;
        }
    }
    return 1;
}


int lanzamiento(int eleccion_player, int apuesta_cpu, int apuesta_player){
	int numero = 0;
	int i = 0;
	srand(time(NULL));
	numero = 1 + rand() % ((2+1)-1);
	for(i=0; i<15; i++){
        aguila();
		Sleep(40);
		system("cls");
		sol();
        Sleep(40);
        system("cls");
    }
    if(numero == 1){
    	aguila();
    }else{
    	sol();
    }
    if (eleccion_player == numero){
    	printf("\n\n\n\t\t\t GANASTE el volado obtienes $%d de CPU",apuesta_cpu);
    }else{
    	printf("\n\n\n\t\t\t FALLASTE el volado pierdes $%d ante CPU",apuesta_player);
    }
    printf("\n\n\t\t              ENTER para continuar");
    getchar();
    getchar();
    system("cls");
    return numero;
}


void reglas(){
    printf("\n\t%c==================================================================%c",201,187);
    printf("\n\t%c                                                                  %c",186,186);
    printf("\n\t%c          _   _  _____  _       ___  ______  _____  _____         %c",186,186);
    printf("\n\t%c         | | | ||  _  || |     / _ \\ |  _  \\|  _  |/  ___|        %c",186,186);
    printf("\n\t%c         | | | || | | || |    / /_\\ \\| | | || | | |\\ `--.         %c",186,186);
    printf("\n\t%c         | | | || | | || |    |  _  || | | || | | | `--. \\        %c",186,186);
    printf("\n\t%c         \\ \\_/ /\\ \\_/ /| |____| | | || |/ / \\ \\_/ //\\__/ /        %c",186,186);
    printf("\n\t%c          \\___/  \\___/ \\_____/\\_| |_/|___/   \\___/ \\____/         %c",186,186);
    printf("\n\t%c                                                                  %c",186,186);
    printf("\n\t%c%c================================================================%c%c\n\n",200,205,205,188);

    printf("\n  Este es un programa para jugar volados,");
    printf("\n  el juego consta de 3 rondas:\n");
    printf("\n    - Al inicio de cada ronda la computadora realizar%c una apuesta aleatoria entre",160);
    printf("\n      $20 y $100, despu%cs tu deberas realizar una apuesta igual entre $20 y $100.",130);
    printf("\n\n    - Tendras que elegir AGUILA o SOL y se lanzar%c el volado,",160);
    printf("\n      si adivinaste ganas, de lo contrario gana el CPU.");
    printf("\n\n    - El ganador se lleva la cantidad de dinero que haya apostado su rival");
    printf("\n      y este restar%c de su bolsa el dinero que perdi%c,",160,162);
    printf("\n\n    - Tanto el jugador como el CPU empiezan con una bolsa de $500.");
    printf("\n      Quien adivine m%cs rondas gana. Al final de las 3 rondas ",160);
    printf("\n      se mostrar%c el ganador y la cantidad de dinero del jugador como del CPU.",160);
    printf("\n\n\n\n\t\t            presione ENTER para jugar");
    getchar();
    system("cls");
}

    
void aguila(){
	printf("\n\n\n\n");
    printf("\t                          ¦¦¦¦¦¦¦¦¦¦                     \n");
    printf("\t                        ¦¦          ¦¦                   \n");
    printf("\t                      ¦¦¦¦¦¦¦¦¦¦¦¦    ¦¦                 \n");
    printf("\t                    ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦    ¦¦               \n");
    printf("\t                    ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦  ¦¦               \n");
    printf("\t                    ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦  ¦¦               \n");
    printf("\t                    ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦  ¦¦               \n");
    printf("\t                    ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦  ¦¦               \n");
    printf("\t                      ¦¦¦¦¦¦¦¦¦¦¦¦¦¦  ¦¦                 \n");
    printf("\t                       ¦¦¦¦¦¦¦¦¦¦¦¦¦¦                   \n");
    printf("\t                          ¦¦¦¦¦¦¦¦¦¦                     \n");
    printf("\t                                                     \n");
    printf("\t              ___   _____  _   _  _____  _       ___     \n");
    printf("\t	     / _ \\ |  __ \\| | | ||_   _|| |     / _ \\    \n");
    printf("\t	    / /_\\ \\| |  \\/| | | |  | |  | |    / /_\\ \\   \n");
    printf("\t	    |  _  || | __ | | | |  | |  | |    |  _  |   \n");
    printf("\t	    | | | || |_\\ \\| |_| | _| |_ | |____| | | |   \n");
    printf("\t	    \\_| |_/ \\____/ \\___/  \\___/ \\_____/\\_| |_/   \n");
}





void sol(){
	printf("\n\n\n\n");
    printf("				      ¦¦¦¦¦¦¦¦¦¦                  \n");
    printf("				    ¦¦          ¦¦                \n");
    printf("				  ¦¦    ¦¦¦¦¦¦¦¦¦¦¦¦              \n");
    printf("				¦¦    ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦            \n");
    printf("				¦¦  ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦            \n");
    printf("				¦¦  ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦            \n");
    printf("				¦¦  ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦            \n");
    printf("				¦¦  ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦            \n");
    printf("				  ¦¦  ¦¦¦¦¦¦¦¦¦¦¦¦¦¦              \n");
    printf("				    ¦¦¦¦¦¦¦¦¦¦¦¦¦¦                \n");
    printf("				      ¦¦¦¦¦¦¦¦¦¦                  \n");
    printf("                                                  \n");
    printf("  				   _____  _____  _                \n");
    printf("				  /  ___||  _  || |               \n");
    printf("				  \\ `--. | | | || |               \n");
    printf("				   `--. \\| | | || |               \n");
    printf("				  /\\__/ /\\ \\_/ /| |____           \n");
    printf("				  \\____/  \\___/ \\_____/           \n");
}


void ganaste(){
    printf("\n         _____            _____   ___   _   _   ___   _____  _ ");
    printf("\n        |_   _|          |  __ \\ / _ \\ | \\ | | / _ \\ /  ___|| |");
    printf("\n          | |   _ //     | |  \\// /_\\ \\|  \\| |/ /_\\ \\\\ `--. | |");
    printf("\n          | |  | | | |   | | __ |  _  || . ` ||  _  | `--. \\| |");
    printf("\n          | |  | |_| |   | |_\\ \\| | | || |\\  || | | |/\\__/ /|_|");
    printf("\n          \\_/   \\__,_|    \\____/\\_| |_/\\_| \\_/\\_| |_/\\____/ (_)");

}


void perdiste(){
    printf("\n         _____ ______  _   _     _____   ___   _   _   ___   _ ");
    printf("\n        /  __ \\| ___ \\| | | |   |  __ \\ / _ \\ | \\ | | / _ \\ | |");
    printf("\n        | /  \\/| |_/ /| | | |   | |  \\// /_\\ \\|  \\| |/ /_\\ \\| |");
    printf("\n        | |    |  __/ | | | |   | | __ |  _  || . ` ||  _  || |");
    printf("\n        | \\__/\\| |    | |_| |   | |_\\ \\| | | || |\\  || | | ||_|");
    printf("\n         \\____/\\_|     \\___/     \\____/\\_| |_/\\_| \\_/\\_| |_/(_)");
}



