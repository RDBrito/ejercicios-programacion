#include <stdio.h>
#include <stdlib.h>

int pide_numero_entero();
int validar_numero_entero(char numero[]);

int main(int argc, char* argv[])
{
    int numero = 0;
    int producto = 0;
    int i = 2;

    printf("\n\n\t%c==================================================================%c",201,187);
    printf("\n\t%c                                                                  %c",186,186);
    printf("\n\t%c    Este programa recibe un n%cmero entero del usuario e imprime   %c",186,163,186);
    printf("\n\t%c          su tabla de multiplicar del 2 al 10.                    %c",186,186);
    printf("\n\t%c                                                                  %c",186,186);
    printf("\n\t%c%c================================================================%c%c\n\n\n",200,205,205,188);
    
    printf("\n  >> Ingrese una n%cmero: ",163);
    numero = pide_numero_entero();

    printf("\n\n  Tabla de multiplicar del %d: \n\n\n", numero);
    for(i = 2; i<=10; i++ ){
        producto = numero * i; 
        printf("\t%d x %d = %d\n",numero,i,producto);
    }

    return 0;
}



//-------Solicita Número Entero al usuario------//
int pide_numero_entero() 
{
   char numero[5];
   int N = 0;
   int numero_valido = 0;
   do
   {
       scanf("%s",numero);
       N=validar_numero_entero(numero);

   }while(N==0);

   numero_valido=atoi(numero);
   return numero_valido;
}

//-------Valida que el numero solo se componga de enteros------//
int validar_numero_entero(char numero[])  
{
    int i = 0;
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i])))
        {
            printf("\n  %c Ingrese %cnicamente un n%cmero entero positivo: ",207,163,163);
            return 0;
        }
    }
    return 1;
}