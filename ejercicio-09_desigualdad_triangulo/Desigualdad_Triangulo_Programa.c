#include <stdio.h>
#include <stdlib.h>


float pide_numero_real();
int validar_numero_real(char numero[]);

int main(int argc, char* argv[])
{
    float segmentoA = 0;
    float segmentoB = 0;
    float segmentoC = 0;
    int bandera = 0;

    printf("\n\n\t%c=================================================================%c",201,187);
    printf("\n\t%c                                                                 %c",186,186);
    printf("\n\t%c    Este programa determina si es posible formar un tri%cngulo    %c",186,160,186);
    printf("\n\t%c         dados tres segmentos ingresados por el usuario          %c",186,186);
    printf("\n\t%c                                                                 %c",186,186);
    printf("\n\t%c%c===============================================================%c%c\n\n\n",200,205,205,188);
    do{
        printf("\n  >> Ingresa la longitud del primer segmento (a): ");
        segmentoA = pide_numero_real();
        printf("\n  >> Ingresa la longitud del segundo segmento (b): ");
        segmentoB = pide_numero_real();
        printf("\n  >> Ingresa la longitud del tercer segmento (c): ");
        segmentoC = pide_numero_real();
        if(segmentoA == 0 || segmentoB == 0 || segmentoC == 0){
            printf("\n\n  Los tres segmentos deben tener longitudes mayores a 0.\n");
        }
    }while( segmentoA == 0 || segmentoB == 0 || segmentoC == 0 );
    if( segmentoA < (segmentoB+segmentoC) ){
        if( segmentoB < (segmentoA+segmentoC) ){
    	    if( segmentoC < (segmentoA+segmentoB) ){
    		    bandera = 0;
            }else{
        	    bandera = -1;
            }
        }else{
            bandera = -1;
        }
    }else{
        bandera = -1;
    }
    
    if( (segmentoA == segmentoB) &&  (segmentoA == segmentoC) && (segmentoB == segmentoC) ){
        bandera = 1;
    }else if( (segmentoA == segmentoB) ||  (segmentoA == segmentoC) || (segmentoB == segmentoC) ){
        bandera = 2;
    }

    switch (bandera){
        case -1:
            printf("\n\n\n  %c No es posible formar un tri%cngulo con la longitud de los segmentos ingresados.\n\n",219,160);
            break;
        case 0:
            printf("\n\n\n  %c Se puede formar un tri%cngulo escaleno con los segmentos ingresados.\n\n",219,160);
            break;
        case 1:
            printf("\n\n\n  %c Se puede formar un tri%cngulo equil%ctero con los segmentos ingresados.\n\n",219,160,160);
            break;
        case 2:
            printf("\n\n\n  %c Se puede formar un tri%cngulo is%csceles con los segmentos ingresados.\n\n",219,160,162);
            break;
    }

    return 0;
}



//Funciones que se encargan de pedir y verificar un número real positivo al usuario
float pide_numero_real()
{
    char numero[5];
    int N;
    float numerovalido;
    do{
        scanf("%s",numero);
        N=validar_numero_real(numero);
    }while(N==0);

    numerovalido=atof(numero);
    return numerovalido;
}

int validar_numero_real(char numero[]){
    int i;
    int punto = 0;
    
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i]))){
            if(numero[i] == '.'){
                punto++;
            }
            else{
        	 printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        	 return 0;
            }
        }
    }
    if(punto >=2){
        printf("\n  %s no es un n%cmero valido",numero,163);
        printf("\n  %c Ingrese %cnicamente n%cmeros reales positivos: ",207,163,163);
        return 0;
    }
    return 1;
}