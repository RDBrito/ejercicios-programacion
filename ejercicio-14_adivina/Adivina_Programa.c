#include <stdio.h>
#include <stdlib.h>

int pide_numero_entero();
int validar_numero_entero(char numero[]);
void perdiste();
void ganaste();

int main(int argc, char* argv[])
{
    int rondas = 5;
    int aleatorio = 0;
    int intento = 0;
    
    printf("\n\n\t%c==================================================================%c",201,187);
    printf("\n\t%c                                                                  %c",186,186);
    printf("\n\t%c    Este programa es un juego de adivinar un n%cmero aleatorio     %c",186,163,186);
    printf("\n\t%c                        entre 1 y 100                             %c",186,186);
    printf("\n\t%c            (solo tienes 5 intentos para adivinar)                %c",186,186);
    printf("\n\t%c                                                                  %c",186,186);
    printf("\n\t%c%c================================================================%c%c\n\n\n",200,205,205,188);
    
    srand(time(NULL));
    aleatorio = 1 + rand() % 101;  //genera números aluatorios del 1 al 100
    do{
//------------- Descomentar la linea de abajo para probar facilmente el correcto funcionamiento   ----------//
        //printf("\n\n  numero aleatorio: %d \n\n",aleatorio); 

        printf("  Intentos restantes - %d \n",rondas);
        do{
            printf("\n  >> Ingrese un n%cmero: ",163);
            intento = pide_numero_entero();
            if( intento < 1   ||  intento > 100 ){
                printf("\n\n  Solo se pueden ingresar n%cmeros del 1 al 100.",163);
            }    	
        }while(  intento < 1   ||  intento > 100 );
        if( intento == aleatorio ){
            printf("\n\n\n  _________________________________________________________________________\n");
            ganaste();
            printf("\n\n\n\n\t\t\t%c==========================%c",201,187);
            printf("\n\t\t\t     El numero es: %d     ",aleatorio);
            printf("\n\t\t\t%c%c========================%c%c\n",200,205,205,188);
            printf("\n  _________________________________________________________________________\n\n\n");
            return 1;
        }else if (rondas != 1){
            printf("\n\n  Fallaste, prueba otra vez.");
        }
        rondas--;
    }while(rondas >= 1);
    printf("\n\n\n  _________________________________________________________________________\n");
    perdiste();
    printf("\n\n\n\n\t\t\t%c==========================%c",201,187);
    printf("\n\t\t\t     El numero era: %d     ",aleatorio);
    printf("\n\t\t\t%c%c========================%c%c\n",200,205,205,188);
    printf("\n  _________________________________________________________________________\n\n\n");

    return 0;    
}



//-------Solicita Número Entero al usuario------//
int pide_numero_entero() 
{
   char numero[5];
   int N = 0;
   int numero_valido = 0;
   do
   {
       scanf("%s",numero);
       N=validar_numero_entero(numero);

   }while(N==0);

   numero_valido=atoi(numero);
   return numero_valido;
}

//-------Valida que el numero solo se componga de enteros------//
int validar_numero_entero(char numero[])  
{
    int i = 0;
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i])))
        {
            printf("\n  %c Ingrese %cnicamente un n%cmero entero positivo: ",207,163,163);
            return 0;
        }
    }
    return 1;
}


//-------Función que imprime el mensaje perdiste------//
void perdiste(){
    printf("\n    _______  _______  _______  ______  _________ _______ _________ _______ ");
    printf("\n   (  ____ )(  ____ \\(  ____ )(  __  \\ \\__   __/(  ____ \\\\__   __/(  ____ \\");
    printf("\n   | (    )|| (    \\/| (    )|| (  \\  )   ) (   | (    \\/   ) (   | (    \\/");
    printf("\n   | (____)|| (__    | (____)|| |   ) |   | |   | (_____    | |   | (__    ");
    printf("\n   |  _____)|  __)   |     __)| |   | |   | |   (_____  )   | |   |  __)   ");
    printf("\n   | (      | (      | (\\ (   | |   ) |   | |         ) |   | |   | (      ");
    printf("\n   | )      | (____/\\| ) \\ \\__| (__/  )___) (___/\\____) |   | |   | (____/\\");
    printf("\n   |/       (_______/|/   \\__/(______/ \\_______/\\_______)   )_(   (_______/");
}


//-------Función que imprime el mensaje ganaste------//
void ganaste(){
    printf("\n        _______  _______  _        _______  _______ _________ _______ ");
    printf("\n       (  ____ \\(  ___  )( (    /|(  ___  )(  ____ \\\\__   __/(  ____ \\");
    printf("\n       | (    \\/| (   ) ||  \\  ( || (   ) || (    \\/   ) (   | (    \\/");
    printf("\n       | |      | (___) ||   \\ | || (___) || (_____    | |   | (__    ");
    printf("\n       | | ____ |  ___  || (\\ \\) ||  ___  |(_____  )   | |   |  __)   ");
    printf("\n       | | \\_  )| (   ) || | \\   || (   ) |      ) |   | |   | (      ");
    printf("\n       | (___) || )   ( || )  \\  || )   ( |/\\____) |   | |   | (____/\\");
    printf("\n       (_______)|/     \\||/    )_)|/     \\|\_______)   )_(   (_______/");
}
