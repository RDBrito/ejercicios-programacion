#include <stdio.h>

int main(int argc, char* argv[])
{
    int numeroPar = 0;

    //Mensaje al usuario
    printf("\n\n\t%c============================================================%c",201,187);
    printf("\n\t%c                                                            %c",186,186);
    printf("\n\t%c    Este programa muestra los n%cmeros pares del 0 al 100.   %c",186,163,186);		
    printf("\n\t%c                                                            %c",186,186);
    printf("\n\t%c%c==========================================================%c%c\n\n",200,205,205,188);
	
    //Ciclo for que imprime los numeros pares del 0 al 100
    for(numeroPar=0; numeroPar<=100; numeroPar=numeroPar+2){
        //if que ayuda a dar formato a la impreci�n de los n�meros pares
        if(numeroPar%20 == 0){
            printf("\n\n\n    ");
        }
        printf("     %d", numeroPar);
    }
    return 0;
}
