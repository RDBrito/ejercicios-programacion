#include <stdio.h>
#include <stdlib.h>

int pide_numero_entero();
int validar_numero_entero(char numero[]);

int main(int argc, char* argv[])
{
    int i = 0;

    printf("\n\n  %c=================================================================================%c",201,187);
    printf("\n  %c                                                                                 %c",186,186);
    printf("\n  %c  Este programa imprime los n%cmeros del 1 al 100, pero aplicando las siguientes  %c",186,163,186);
    printf("\n  %c  reglas.                                                                        %c",186,186);
    printf("\n  %c                                                                                 %c",186,186);
    printf("\n  %c   -Regla 1: Cuando el n%cmero sea divisible entre 3, en vez del n%cmero           %c",186,163,163,186);
    printf("\n  %c             se escribir%c 'ping'                                                 %c",186,160,186);
    printf("\n  %c                                                                                 %c",186,186);
    printf("\n  %c   -Regla 2: Cuando el n%cmero sea divisible entre 5, en vez del n%cmero           %c",186,163,163,186);
    printf("\n  %c             se escribir%c 'pong'                                                 %c",186,160,186);
    printf("\n  %c                                                                                 %c",186,186);
    printf("\n  %c   -Regla 3: Cuando el n%cmero sea divisible entre 3 y tambi%cn divisible          %c",186,163,130,186);
    printf("\n  %c             entre 5, en vez del n%cmero se escribir%c 'ping-pong'                 %c",186,163,160,186);
    printf("\n  %c                                                                                 %c",186,186);
    printf("\n  %c%c===============================================================================%c%c\n\n\n\n",200,205,205,188);
    
    printf("  _______________________________\n\n");
    for(i = 1; i<=100;i++){
        if( (i%3 == 0) && (i%5 == 0) ){
            printf("\t    ping-pong\n");
        }else if(i%3 == 0){
            printf("\t    ping\n");
        }else if(i%5 == 0){
            printf("\t    pong\n");
        }else{
            printf("\t    %d\n",i);
        }
    }
    printf("  _______________________________\n\n\n");
    return 0;    
}