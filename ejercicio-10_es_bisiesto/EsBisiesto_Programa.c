#include <stdio.h>
#include <stdlib.h>

int pide_numero_entero();
int validar_numero_entero(char numero[]);

int main(int argc, char* argv[])
{
    int anio = 0;

    printf("\n\n\t%c==================================================================%c",201,187);
    printf("\n\t%c                                                                  %c",186,186);
    printf("\n\t%c    Este programa recibe un n%cmero entero que repesente un a%co    %c",186,163,164,186);
    printf("\n\t%c              y determinar%c si el a%co es bisiesto.                %c",186,160,164,186);
    printf("\n\t%c                                                                  %c",186,186);
    printf("\n\t%c%c================================================================%c%c\n\n\n",200,205,205,188);
    
    do{
        printf("\n  >> Ingrese un a%co: ",164);
        anio = pide_numero_entero();
        if(anio == 0){
            printf("\n\n  El m%cnimo a%co posible es 1.",161,164);
        }    	
    }while(  anio == 0 );   

    if(anio%4 == 0){
        if(anio%100 == 0){
            if(anio%400 == 0){
                printf("\n\n  El a%co %d es bisiesto. \n\n",164,anio);   		
                return 1;
            }else{
                printf("\n\n  %d NO es a%co bisiesto. \n\n",anio,164);
                return 0;    			
            }
        }else{
            printf("\n\n  El a%co %d es bisiesto. \n\n",164,anio);   		
            return 1;
        }
    }else{
        printf("\n\n  %d NO es a%co bisiesto. \n\n",anio,164);
        return 0;
    }
    
}

//-------Solicita Número Entero al usuario------//
int pide_numero_entero() 
{
   char numero[5];
   int N = 0;
   int numero_valido = 0;
   do
   {
       scanf("%s",numero);
       N=validar_numero_entero(numero);

   }while(N==0);

   numero_valido=atoi(numero);
   return numero_valido;
}

//-------Valida que el numero solo se componga de enteros------//
int validar_numero_entero(char numero[])  
{
    int i = 0;
    for(i=0; i<strlen(numero); i++)
    {
        if(!(isdigit(numero[i])))
        {
            printf("\n  %c Ingrese %cnicamente un n%cmero entero positivo: ",207,163,163);
            return 0;
        }
    }
    return 1;
}